#!/bin/sh
SCRIPT_SOURCE=$(dirname "$0")
. "${SCRIPT_SOURCE}"/test.sh
. "${SCRIPT_SOURCE}"/assertions.sh

test_should_run_the_test () {
  test_to_run () {
    echo "Test ran"
  }
  result=$(test test_to_run)
  expect_match "${result}" "Test ran"
  return $?
}; test test_should_run_the_test

test_should_echo_the_name_of_the_test () {
  result=$(test true)
  expect_match "${result}" "true"
  return $?
}; test test_should_echo_the_name_of_the_test

test_should_echo_OK_when_success () {
  result=$(test true)
  expect_match "${result}" 'OK'
  return $?
}; test test_should_echo_OK_when_success

test_should_echo_KO_when_failure () {
  result=$(test false)
  expect_match "${result}" 'KO'
  return $?
}; test test_should_echo_KO_when_failure

end_testing_should_fail_when_a_test_fails () {
  (test true; test false; test true; end_testing) > /dev/null
  expect_failure $?
  return $?
}; test end_testing_should_fail_when_a_test_fails

end_testing_should_succeed_when_all_tests_pass () {
  (test true; end_testing) > /dev/null
  expect_success $?
  return $?
}; test end_testing_should_succeed_when_all_tests_pass

end_testing_should_succeed_when_no_tests_are_found () {
  (end_testing) > /dev/null
  expect_success $?
  return $?
}; test end_testing_should_succeed_when_no_tests_are_found

end_testing_should_reset_the_exit_code () {
  (test false; end_testing; end_testing) > /dev/null
  expect_success $?
  return $?
}; test end_testing_should_reset_the_exit_code

end_testing