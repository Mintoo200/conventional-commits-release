#!/bin/sh
SCRIPT_SOURCE=$(dirname "$0")
. "${SCRIPT_SOURCE}"/test.sh
. "${SCRIPT_SOURCE}"/assertions.sh

expect_equal_should_return_1_when_not_equal () {
  expect_equal 1 2
  result=$?
  [ ${result} = 1 ]
  return $?
}; test expect_equal_should_return_1_when_not_equal

expect_equal_should_return_0_when_equal () {
  expect_equal 1 1
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_equal_should_return_0_when_equal

expect_equal_should_return_1_when_only_one_value_is_empty () {
  expect_equal 1
  result=$?
  [ ${result} = 1 ]
  return $?
}; test expect_equal_should_return_1_when_only_one_value_is_empty

expect_equal_should_return_0_when_both_values_are_empty () {
  expect_equal
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_equal_should_return_0_when_both_values_are_empty

expect_success_should_return_0_when_is_success () {
  true
  expect_success $?
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_success_should_return_0_when_is_success

expect_success_should_return_1_when_is_failure () {
  false
  expect_success $?
  result=$?
  [ ${result} = 1 ]
  return $?
}; test expect_success_should_return_1_when_is_failure

expect_failure_should_return_0_when_is_failure () {
  false
  expect_failure $?
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_failure_should_return_0_when_is_failure

expect_failure_should_return_1_when_is_success () {
  true
  expect_failure $?
  result=$?
  [ ${result} = 1 ]
  return $?
}; test expect_failure_should_return_1_when_is_success

expect_match_should_return_0_when_value_partially_matches () {
  expect_match "Hello" "ell"
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_match_should_return_0_when_value_partially_matches

expect_match_should_return_0_when_value_fully_matches () {
  expect_match "Hello" "Hello"
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_match_should_return_0_when_value_fully_matches

expect_match_should_return_0_when_value_matches_regex () {
  expect_match "Hello" "^He..o$"
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_match_should_return_0_when_value_matches_regex

expect_match_should_return_1_when_value_does_not_matches () {
  expect_match "Hello" "Goodbye"
  result=$?
  [ ${result} = 1 ]
  return $?
}; test expect_match_should_return_1_when_value_does_not_matches

expect_match_should_return_0_when_value_has_multiple_parts_and_matches () {
  expect_match "H el \n lo" "lo"
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_match_should_return_0_when_value_has_multiple_parts_and_matches

expect_match_should_return_0_when_matcher_has_multiple_parts_and_matches () {
  expect_match "Hel lo" "l lo"
  result=$?
  [ ${result} = 0 ]
  return $?
}; test expect_match_should_return_0_when_matcher_has_multiple_parts_and_matches

end_testing