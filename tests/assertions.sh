#!/bin/sh

expect_equal () {
  [ -z "$1" ] || [ "$2" ] && [ "$1" = "$2" ]
  return $?
}

expect_success () {
  expect_equal "$1" 0
  return $?
}

expect_failure () {
  expect_success "$1"
  expect_equal $? 1
  return $?
}

expect_match () {
  echo "$1" | grep -Eq "$2"
  return $?
}