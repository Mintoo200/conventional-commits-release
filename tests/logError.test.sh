#!/bin/sh
SCRIPT_SOURCE=$(dirname "$0")
. "${SCRIPT_SOURCE}"/test.sh
. "${SCRIPT_SOURCE}"/assertions.sh
. "${SCRIPT_SOURCE}"/../scripts/logError.sh

log_error_should_log_on_stderr () {
  stderr=$(log_error "Error!" 2>&1 1> /dev/null)
  expect_match "${stderr}" "Error!"
  return $?
}; test log_error_should_log_on_stderr

end_testing