#!/bin/sh

EXIT_CODE=0

SUCCESS_STYLE="\033[1m\033[92m"
FAILURE_STYLE="\033[1m\033[5m\033[91m"
RESET_STYLE="\033[0m"

clear_line () {
  printf "\33[2K\r";
}

test () {
  fn=$1
  printf "Running %s ..." "$fn"
  ${fn}
  LOCAL_EXIT_CODE=$?
  clear_line
  if [ ${LOCAL_EXIT_CODE} = 0 ]; then
    echo "${SUCCESS_STYLE}✅${RESET_STYLE} ${fn} ${SUCCESS_STYLE}OK${RESET_STYLE}"
  else
    echo "${FAILURE_STYLE}❌${RESET_STYLE} ${fn} ${FAILURE_STYLE}KO${RESET_STYLE}"
    EXIT_CODE=${LOCAL_EXIT_CODE}    
  fi
}

end_testing () {
  LOCAL_EXIT_CODE=${EXIT_CODE}
  EXIT_CODE=0
  return ${LOCAL_EXIT_CODE}
}
