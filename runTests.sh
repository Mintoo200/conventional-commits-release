#!/bin/sh
SCRIPT_SOURCE=$(dirname "$0")
. "${SCRIPT_SOURCE}"/tests/test.sh
. "${SCRIPT_SOURCE}"/tests/assertions.sh

run_all_tests () {
  result=0
  for FILE in "${SCRIPT_SOURCE}"/tests/*.test.sh; do
    echo "${FILE}: "
    ${FILE}
    result=$((result | $?))
    echo
  done
  return ${result}
}; run_all_tests